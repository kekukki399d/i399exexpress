'use strict';

class Task {
    constructor(content) {
        this.content = content;
    }

    static withId(id, title) {
        let task = new Task(title);
        task._id = id;
        return task;
    }
}

module.exports = Task;