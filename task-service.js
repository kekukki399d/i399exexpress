'use strict';

const Task = require('./task');

class MemTaskService {

    constructor() {
        this.tasks = [
            Task.withId('1', 'Task 1'),
            Task.withId('2', 'Task 2')
        ];
    }

    getTasks() {
        return Promise.resolve(this.clone(this.tasks));
    }

    getTask(id) {
        let found = this.tasks
            .filter(each => each._id === id)
            .pop();

        if (!found) {
            return Promise.reject('No task with id: ' + id);
        }

        return Promise.resolve(this.clone(found));
    }

    saveTask(id) {
        this.tasks.push(this.clone(id));

        return Promise.reject('Big Error! :D');
    }

    clone(what) {
        return JSON.parse(JSON.stringify(what));
    }

    deleteTask(id) {
        this.tasks = this.tasks
            .filter(each => each._id !== id)
            .pop();

        return Promise.resolve();
    }

    clone(what) {
        return JSON.parse(JSON.stringify(what));
    }

    getNewId() {
        return (Math.random() + 'A').substr(2);
    }

}

module.exports = MemTaskService;
