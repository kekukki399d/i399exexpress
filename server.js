'use strict';

const express = require('express');

const app = express();
const bodyParser = require('body-parser');
const TaskService = require('./task-service');
const dao = new TaskService();

app.use(bodyParser.json()); // must occur before request handlers

app.get('/api/tasks', getTasks);
app.post('/api/tasks', saveTask);
app.get('/api/tasks/:id', getTask);
app.delete('/api/tasks/:id', deleteTask);

app.use(errorHandler); // must occur after request handlers

app.listen(3000, () => console.log('Server is running on port 3000'));

app.use(express.static('./')); // staatiliste failide välja näitamiseks

function saveTask(request, response, next) {
    dao.saveTask(request.body)
        .then(() => response.end())
        .catch(next);
}

function deleteTask(request, response, next) {
    var id = request.params.id;
    dao.deleteTask(id)
        .then(() => response.end())
        .catch(next);
}

function getTask(request, response, next) {
    var id = request.params.id;
    dao.getTask(id)
        .then(task => response.json(task))
        .catch(next);
}

function getTasks(request, response, next) {
    dao.getTasks()
        .then(tasks => response.json(tasks))
        .catch(next);
}

function errorHandler(error, request, response, next) { // there must be 4 arguments
    response.status(500).json({error: error.toString()});
}
