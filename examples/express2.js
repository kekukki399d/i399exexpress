'use strict';

const express = require('express');
const app = express();

app.use(express.static('./'));

app.get('/api/tasks', getTasks);
app.get('/api/tasks/:id', getTask);

app.listen(3000, () => console.log('Server is running...'));

function getTasks(request, response) {
    response.set('Content-Type', 'application/json');
    response.end(JSON.stringify([{ id: 1 }, { id: 2 }]));
}

function getTask(request, response) {
    var id = request.params.id;
    response.set('Content-Type', 'application/json');
    response.end(JSON.stringify({ id: id }));
}
