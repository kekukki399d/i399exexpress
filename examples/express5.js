'use strict';

const express = require('express');
const app = express();

app.get('/api/p', p1);
app.get('/api/p', p2);

app.use(errorHandler); // after request handlers

app.listen(3000, () => console.log('Server is running...'));

function p1(request, response, next) {
    console.log('p1');
    next();
}

function p2(request, response, next) {
    console.log('p2');
    next('error from p2'); // argument makes it an error
}

function errorHandler(error, request, response, next) { // there must be 4 arguments
    response.status(500).send(error.toString());
}
