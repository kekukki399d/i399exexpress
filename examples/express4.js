'use strict';

const express = require('express');
const app = express();

app.get('/api/p1', p1);
app.get('/api/p2', p2);
app.get('/api/p3', p3);

app.use(errorHandler); // after request handlers

app.listen(3000, () => console.log('Server is running...'));

function p1(request, response) {
    try {
        throw new Error('p1');
    } catch (error) {
        response.status(500).send(error.toString());
    }

}

function p2(request, response) {
    try {
        throw new Error('p2');
    } catch (error) {
        response.status(500).send(error.toString());
    }
}

function p3(request, response) {
    throw new Error('p3');
}

function errorHandler(error, request, response, next) { // there must be 4 arguments
    response.status(500).send(error.toString());
}
