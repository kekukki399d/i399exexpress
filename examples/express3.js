'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json()); // before request handlers

app.get('/api/tasks', getTasks);
app.post('/api/tasks', saveTask);

app.listen(3000, () => console.log('Server is running...'));

function getTasks(request, response) {
    response.json([{ id: 1 }, { id: 2 }]);
}

function saveTask(request, response) {
    console.log(request.body); // request should have correct content-type
    response.end();
}
